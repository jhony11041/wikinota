package main

import (
	"fmt"
	"time"
)

var authenticator_cahce1 = make(map[string]string)
var authenticator_cahce2 = make(map[string]string)
var authenticator_cahce3 = make(map[string]string)

var authenticator_name_cahce1 = make(map[string]int)
var authenticator_name_cahce2 = make(map[string]int)
var authenticator_name_cahce3 = make(map[string]int)

var authenticator_cahce1_lock bool = false
var authenticator_cahce2_lock bool = false
var authenticator_cahce3_lock bool = false

var authenticatorFILL_lock bool = false

func authenticator(UserName string, PWD string) (login bool) {

	var chachedHash string

	if authenticator_cahce1_lock == false {
		chachedHash = authenticator_cahce1[UserName]
	} else {
		if authenticator_cahce2_lock == false {
			chachedHash = authenticator_cahce2[UserName]
		} else {
			if authenticator_cahce3_lock == false {
				chachedHash = authenticator_cahce3[UserName]
			} else {
				DebugOutput("!!!!!! ERROR - authenticator HAS NO UNLOCKED CACHE !!!!!!")
				return
			}
		}
	}

	if chachedHash == PWD {
		login = true
		return
	}

	login = false
	return

}

func authenticatorFILL() {
	if authenticatorFILL_lock == true {
		return
	}
	authenticatorFILL_lock = true

	ids, err := db.Query(sqlc.Authenticator_gethashes)

	if err != nil {
		DebugOutput("authenticatorFILL", err)
	}

	defer ids.Close()

	var authenticatorTMP = make(map[string]string)
	var authenticator_nameTMP = make(map[string]int)

	for ids.Next() {
		var id int
		var username, pwd string
		err = ids.Scan(&id, &username, &pwd)
		checkErr(err)
		authenticatorTMP[username] = pwd
		authenticator_nameTMP[username] = id

	}

cache1:
	if authenticator_cahce1_lock == false {
		authenticator_cahce1_lock = true
		time.Sleep(500 * time.Millisecond)
		DebugOutput("authenticatorFILL - Loading Cache1")
		authenticator_cahce1 = authenticatorTMP
		authenticator_name_cahce1 = authenticator_nameTMP
		authenticator_cahce1_lock = false
	} else {
		DebugOutput("authenticatorFILL - chach1 locked _ wait 1 sec")
		time.Sleep(1 * time.Second)
		goto cache1
	}

cache2:
	if authenticator_cahce2_lock == false {
		authenticator_cahce2_lock = true
		time.Sleep(500 * time.Millisecond)
		DebugOutput("authenticatorFILL - Loading Cache2")
		authenticator_cahce2 = authenticatorTMP
		authenticator_name_cahce2 = authenticator_nameTMP
		authenticator_cahce2_lock = false
	} else {
		DebugOutput("authenticatorFILL - chach2 locked _ wait 1 sec")
		time.Sleep(1 * time.Second)
		goto cache2
	}

cache3:
	if authenticator_cahce3_lock == false {
		authenticator_cahce3_lock = true
		time.Sleep(500 * time.Millisecond)
		DebugOutput("authenticatorFILL - Loading Cache3")
		authenticator_cahce3 = authenticatorTMP
		authenticator_name_cahce3 = authenticator_nameTMP
		authenticator_cahce3_lock = false
	} else {
		DebugOutput("authenticatorFILL - chach3 locked _ wait 1 sec")
		time.Sleep(1 * time.Second)
		goto cache3
	}

	fmt.Println("authenticatorFILL CHACHE FINISHED \nauthenticatorFILL: - ", len(authenticatorTMP), " - HASH's are saved in 3 chaches")

	authenticatorFILL_lock = false

}

func NameToID(name string) (id int) {

	if authenticator_cahce1_lock == false {
		id = authenticator_name_cahce1[name]
		return
	} else {
		if authenticator_cahce2_lock == false {
			id = authenticator_name_cahce2[name]
			return
		} else {
			if authenticator_cahce3_lock == false {
				id = authenticator_name_cahce3[name]
				return
			} else {
				fmt.Println("!!!!!! ERROR - authenticator NAME HAS NO UNLOCKED CACHE !!!!!!")
				return
			}
		}
	}

}
