# wikinota
wikinota is a selfhosted platform for you ideas and thoughts.
The main focus is set to archiving and inspire new knowledge, through the structuring of thought paths.

For this purpose, a new structure of the page hierarchy will be implemented soon.
This structure will be inspired by the [Zettelkasten*](https://de.wikipedia.org/wiki/Zettelkasten) model.

The second focus is to provide a way to search through this structure of pages, to find relevant informations.

The third focus is a collection of handy tools for evryday life. Like the "Geldlog" (moneylog)

\* This wikipedia article is in german only - pls. help to translate it

## Functions
- Wikipedia Like Pages with Markdown
- Moneylog (called Geldlog)
- Searchable
- Easy API (this API will be changed heavily in future updates)


## Installation
You need to install Golang and Mysql.  
Pull the git repo and the submodule.    
set gopath and enter `go get ./...`  
config the config.toml  
start the wiki with `go run *.go`

- A better install guide will be writen soon  

![alt text](https://github.com/i5heu/Wikinota-Content/blob/master/wikinota-demo-smal2.gif "Wikinota Demo")



## Versions

Version 3.1 is considered to be a production ready release.
