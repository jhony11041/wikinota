//This is the service worker with the Cache-first network

var CACHE = 'pwabuilder-precache';
var precacheFiles =   [
    '/static/css/main.css',
    '/static/favicon/favicon.ico',
    '/static/js/DeleteManager.js',
    '/static/js/desk.js',
    '/static/js/geldlog.js',
    '/static/js/OffSync.js',
    '/static/js/search.js',
    '/static/js/pedit.js',
    '/static/js/vue.min.js',
    '/static/js/main.js',
    '/web#/',
    'https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css',
    'https://cdn.jsdelivr.net/gh/i5heu/GoodReadCSS@3/GoodReadCSS.min.css',
    'https://cdn.jsdelivr.net/npm/jshashes@1/hashes.min.js',
    'https://cdn.jsdelivr.net/npm/pouchdb@6.3.4/dist/pouchdb.min.js',
    'https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.min.js',
    'https://cdn.jsdelivr.net/npm/vue-router@3.0.1/dist/vue-router.min.js',
    'https://cdn.jsdelivr.net/npm/vue-focus@2.1.0/dist/vue-focus.common.min.js',
    'https://cdn.jsdelivr.net/npm/vue-resource@1.3.4',
    'https://cdn.jsdelivr.net/npm/marked@0.3.6/lib/marked.min.js',
    'https://cdn.jsdelivr.net/npm/moment@2.19.1/moment.min.js',
    'https://fonts.googleapis.com/css?family=Ubuntu:300,400'
  ];


  //Install stage sets up the cache-array to configure pre-cache content
  self.addEventListener('install', function(evt) {
    console.log('The service worker is being installed.');
    evt.waitUntil(precache().then(function() {
      console.log('[ServiceWorker] Skip waiting on install');
        return self.skipWaiting();

    })
    );
  });


  //allow sw to control of current page
  self.addEventListener('activate', function(event) {
  console.log('[ServiceWorker] Claiming clients for current page');
        return self.clients.claim();

  });

  self.addEventListener('fetch', function(evt) {
    if(evt.request.url.includes("api") || evt.request.url.includes("?")){
      console.log('The SERVER is serving the asset.'+ evt.request.url);
        evt.respondWith(fromServer(evt.request));
        evt.waitUntil(update(evt.request));
    }else{
      console.log('The service worker is serving the asset.'+ evt.request.url);
        evt.respondWith(fromCache(evt.request).catch(fromServer(evt.request)));
      evt.waitUntil(update(evt.request));
    }
  });


  function precache() {
    return caches.open(CACHE).then(function (cache) {
      return cache.addAll(precacheFiles);
    });
  }


  function fromCache(request) {
    //we pull files from the cache first thing so we can show them fast
    return caches.open(CACHE).then(function (cache) {
      return cache.match(request).then(function (matching) {
        return matching || Promise.reject('no-match');
      });
    });
  }


  function update(request) {
      if(!request.url.includes("api") && !request.url.includes("?")){
        //this is where we call the server to get the newest version of the
        //file to use the next time we show view
        return caches.open(CACHE).then(function (cache) {
          return fetch(request).then(function (response) {
            return cache.put(request, response);
          });
        });
    }
  }

  function fromServer(request){
    //this is the fallback if it is not in the cahche to go to the server and get it
  return fetch(request).then(function(response){ return response})
  }
