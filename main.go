package main

import (
	"crypto/sha512"
	"database/sql"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/NYTimes/gziphandler"
	_ "github.com/go-sql-driver/mysql"
)

var WNversion string = "WN-V2.0.1.3" //The Wikinota Version of this CODE

var AdminNotLogin string = `{"Status":"Not Loged In As Admin"}`

var db *sql.DB // Global sql.DB to access the database by all handlers

type Config struct { // data from conf.toml
	WikiUrl                  string
	Dblogin                  string
	Guestmode                bool
	AdminPWD                 string
	GuestPWD                 string
	Templatefolder           string
	AdminHASH                string
	DebugOutput              bool
	JabberNotification       bool
	JabberHost               string
	JabberUser               string
	JabberPassword           string
	JabberServerName         string
	JabberJIDreciever        string
	JabberInsecureSkipVerify bool
}

var conf Config

var fs = http.FileServer(http.Dir("webapp"))

var favicon io.Reader
var indexCache []byte
var ServiceWorkerCache []byte

func main() {
	var err error
	fmt.Println("Prepare")

	if _, err := toml.DecodeFile("config.toml", &conf); err != nil { //Parse the Config from config.toml
		fmt.Println(err)
		return
	}

	//Load Favicon
	favicon, faverr := os.Open("webapp/favicon/favicon.ico")
	if faverr != nil {
		fmt.Println("FAVICON OPEN ERROR - ", faverr)
	}
	defer favicon.Close()

	indexCache, err = ioutil.ReadFile("./webapp/index.html") // THIS CAN BE A GLOBAL VAR (CACHING)
	if err != nil {
		fmt.Println(err)
	}

	ServiceWorkerCache, err = ioutil.ReadFile("./webapp/js/pwabuilder-sw.js") // THIS CAN BE A GLOBAL VAR (CACHING)
	if err != nil {
		fmt.Println(err)
	}

	//Hash pwd
	var foo = sha512.Sum512([]byte(conf.AdminPWD)) //sha256 Parser for Password Token
	conf.AdminHASH = hex.EncodeToString(foo[:])

	// fill sqlc with the sql commands needed for selectet DB
	FillSqlComands()

	mysqlOpenLink := conf.Dblogin + "?parseTime=true"

	// Create an sql.DB and check for errors
	db, err = sql.Open("mysql", mysqlOpenLink)
	checkErr(err)
	db.SetConnMaxLifetime(time.Second * 2)
	db.SetMaxIdleConns(10)
	db.SetMaxOpenConns(25)

	// Open doesn't open a connection. Validate DSN data:
	err = db.Ping()
	checkErr(err)
	// sql.DB should be long lived "defer" closes it once this function ends
	defer db.Close()

	CreateTable() //fill sqlc with variables

	fmt.Println("AdminHash:", conf.AdminHASH)
	fmt.Println("START")

	go func() {
		for {
			time.Sleep(500 * time.Millisecond)

			authenticatorFILL()

			time.Sleep(180 * time.Second)
		}
	}()

	if conf.DebugOutput == true {
		DebugInitiator()
	}

	http.Handle("/api", gziphandler.GzipHandler(HttpBridge)) //proxy HttpBridge through GZiper
	http.Handle("/multiapi", gziphandler.GzipHandler(MultiHttpBridge))
	http.Handle("/static/", http.StripPrefix("/static/", fs)) // FileServer
	http.Handle("/web", gziphandler.GzipHandler(WebAppHandler))
	http.Handle("/pwabuilder-sw.js", gziphandler.GzipHandler(ServiceWorkerHandler))
	http.Handle("/favicon.ico", gziphandler.GzipHandler(FaviconHandler)) //Redirect to location of favicon
	http.HandleFunc("/", IndexHandler2)                                  //Redirect to /web
	fmt.Println(http.ListenAndServe(":8080", nil))

	fmt.Println("END")
}

func dsgvoAuth(w http.ResponseWriter, r *http.Request) bool {

	var cookie, err = r.Cookie("webpwd")
	if err != nil {
		http.Redirect(w, r, conf.WikiUrl+"/static/login.html", 302)
		fmt.Println("Redirecting to ->", "LOGINPAGE")
		return false
	} else {
		if cookie.Value != "bittelassmichreinBerlin" {
			http.Redirect(w, r, conf.WikiUrl+"/static/login.html", 302)
			fmt.Println("Redirecting to ->", "LOGINPAGE")
			return false
		}

	}
	return true
}

var WebAppHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	if dsgvoAuth(w, r) == false {
		return
	}

	w.Header().Set("Content-Type", "text/html;charset=utf-8")
	w.Header().Set("Cache-Control", "max-age=86400")
	w.Write(indexCache)

})

var ServiceWorkerHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	if dsgvoAuth(w, r) == false {
		return
	}

	w.Header().Set("Content-Type", "application/javascript")
	w.Header().Set("Cache-Control", "max-age=86400")
	w.Write(ServiceWorkerCache)

})

func IndexHandler2(w http.ResponseWriter, r *http.Request) {
	startAPI := time.Now() //Take timestamp fo time-masurement

	if dsgvoAuth(w, r) == false {
		return
	}

	keys, ok := r.URL.Query()["s"]
	surl := "/web#/"

	if ok && len(keys[0]) > 1 {

		surl = conf.WikiUrl + "/web#/s/" + keys[0]

		if strings.HasPrefix(keys[0], "!") { //ddg bang search
			surl = "https://duckduckgo.com/?q=" + keys[0]
		}
		if strings.HasPrefix(keys[0], "! ") { //ddg search
			surl = "https://duckduckgo.com/?q=" + keys[0][2:len(keys[0])]
		}
		if strings.HasPrefix(keys[0], "!g") { //google search
			surl = "https://google.de/search?q=" + keys[0][2:len(keys[0])]
		}
	}

	http.Redirect(w, r, surl, 302)

	fmt.Println("IndexHandler2:", time.Since(startAPI), "Redirecting to ->", surl) //output time Measurementm
	return
}

var FaviconHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	if dsgvoAuth(w, r) == false {
		return
	}

	w.Header().Set("Content-Type", "image/jpeg")
	w.Header().Set("Cache-Control", "max-age=86400")
	io.Copy(w, favicon)
})

func checkErr(err error) {
	if err != nil {
		fmt.Println("\033[0;31m", err, "\033[0m")
		//foo := "WikiERR:\n" + err.Error()
		//sendXMPP(foo)
		err = nil
	}
}
