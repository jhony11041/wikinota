package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"time"
)

type MysqlItemStruct struct {
	UserId        int
	Path          string
	Overwrite     bool
	Timecreate    time.Time
	Timelasteedit time.Time
	Public        bool
	APP           string
	viewcounter   int
	editcounter   int
	Title1        string
	Title2        string
	Text1         string
	Text2         string
	Tags1         string
	Num1          float64
	Num2          float64
	Num3          float64
	NoSqlData     []byte
}

func delete_item(jsonreq ReqStruct) (jsonres ResStruct) {

	Path := InterfaceNilChek(&jsonres, jsonreq, "Path", "string").(string)

	if jsonres.Error != false {
		ErrorAPIBuilder(errors.New("Error in Json API"), &jsonres)
	}

	BackupItem(db, NameToID(jsonreq.UserName), Path)

	_, err := db.Exec(sqlc.ApiItemDelete, NameToID(jsonreq.UserName), Path)

	if err != nil {
		ErrorAPIBuilder(err, &jsonres)
		return
	}

	jsonres.DATA = ASimpleStatus{
		Status: "deleted",
	}

	fmt.Println("XXXXXXXXXXXXXXXXXXXXXXXXX", Path)

	return
}

func create_item(jsonreq ReqStruct) (jsonres ResStruct) {

	Path := InterfaceNilChek(&jsonres, jsonreq, "Path", "string").(string)
	APP := InterfaceNilChek(&jsonres, jsonreq, "APP", "string").(string)
	Path, err := CreatePath(Path, APP)

	if err != nil {
		ErrorAPIBuilder(err, &jsonres)
		return
	}
	DebugOutput("PathName:", Path)

	// err = BackupItem(db, Path)
	// if err != nil {
	// 	ErrorAPIBuilder(err, &jsonres)
	// 	return
	// }

	if jsonreq.DATA["NoSqlData"] == nil { //filling for Marshal so its not a error
		jsonreq.DATA["NoSqlData"] = "{}"
	}

	datajson, err := json.Marshal(jsonreq.DATA["NoSqlData"])
	if err != nil {
		ErrorAPIBuilder(err, &jsonres)
		return
	}

	jsondata := MysqlItemStruct{
		UserId:    NameToID(jsonreq.UserName),
		Path:      Path,
		Overwrite: InterfaceNilChek(&jsonres, jsonreq, "Overwrite", "bool").(bool),
		APP:       APP,
		Title1:    InterfaceNilChek(&jsonres, jsonreq, "Title1", "string").(string),
		Title2:    InterfaceNilChek(&jsonres, jsonreq, "Title2", "string").(string),
		Text1:     InterfaceNilChek(&jsonres, jsonreq, "Text1", "string").(string),
		Text2:     InterfaceNilChek(&jsonres, jsonreq, "Text2", "string").(string),
		Tags1:     InterfaceNilChek(&jsonres, jsonreq, "Tags1", "string").(string),
		Num1:      InterfaceNilChek(&jsonres, jsonreq, "Num1", "float64").(float64),
		Num2:      InterfaceNilChek(&jsonres, jsonreq, "Num2", "float64").(float64),
		Num3:      InterfaceNilChek(&jsonres, jsonreq, "Num3", "float64").(float64),
		Public:    InterfaceNilChek(&jsonres, jsonreq, "Public", "bool").(bool),
		NoSqlData: datajson,
	}

	if jsonres.Error != false {

		ErrorAPIBuilder(errors.New("Error in Json API"), &jsonres)
	}

	if jsondata.Overwrite == true {
		BackupItem(db, NameToID(jsonreq.UserName), Path)
		DebugOutput("UPDATE ITEM -- PATH: ", jsondata.Path, " --USERID-- ", jsondata.UserId)
		_, err = db.Exec(sqlc.ApiItemUpdate, jsondata.APP, jsondata.Title1, jsondata.Title2, jsondata.Text1, jsondata.Text2, jsondata.Tags1, jsondata.Num1, jsondata.Num2, jsondata.Num3, jsondata.Public, jsondata.NoSqlData, jsondata.UserId, jsondata.Path)
	} else {
		DebugOutput("INSERT ITEM -- PATH: ", jsondata.Path, " --USERID-- ", jsondata.UserId)
		_, err = db.Exec(sqlc.ApiItemWrite, jsondata.UserId, jsondata.Path, jsondata.APP, jsondata.Title1, jsondata.Title2, jsondata.Text1, jsondata.Text2, jsondata.Tags1, jsondata.Num1, jsondata.Num2, jsondata.Num3, jsondata.Public, jsondata.NoSqlData)
	}

	if err != nil {
		ErrorAPIBuilder(err, &jsonres)
	} else {
		jsonres.DATA = map[string]interface{}{
			"Path": Path,
		}
	}

	return
}

func list(jsonreq ReqStruct) (jsonres ResStruct) {
	var err error
	var ids *sql.Rows

	if debugtest, ok := jsonreq.DATA["ListModule"].(string); !ok {
		DebugOutput("LISTMODUEL CONV TEST:", debugtest, "--", ok)
		ErrorAPIBuilder(errors.New("ListModule is not a string"), &jsonres)
		return
	}

	ids, err = ListModules(jsonreq) // creating the mysql row from ListModules selector

	if err != nil {
		ErrorAPIBuilder(err, &jsonres)
		return
	}

	if ids == nil {
		ErrorAPIBuilder(errors.New("CRITICAL ERROR: ids == nil"), &jsonres)
		return
	}

	defer ids.Close()

	var data []map[string]interface{}
	for ids.Next() {
		var timecreate, timelastedit time.Time
		var path, app, title1, title2, text1, text2, tags1 string
		var num1, num2, num3, score sql.NullFloat64
		var public bool
		var NoSqlData *json.RawMessage

		var RowStatus error

		var sqldata map[string]interface{}

		switch jsonreq.DATA["ListModule"] {
		case "GeldlogAll":
			var SumALL, CurrentMonth, CurrentMonthFood, Last7Days sql.NullFloat64
			RowStatus = ids.Scan(&SumALL, &CurrentMonth, &CurrentMonthFood, &Last7Days)
			DebugOutput("API-Methods:IDS:-> ", RowStatus)
			sqldata = map[string]interface{}{
				"SumALL":           SumALL,
				"CurrentMonth":     CurrentMonth,
				"CurrentMonthFood": CurrentMonthFood,
				"Last7Days":        Last7Days,
			}
		case "ListPathMainSection":
			RowStatus = ids.Scan(&path)
			DebugOutput("API-Methods:IDS:-> ", RowStatus)
			sqldata = map[string]interface{}{
				"Sections": path,
			}
		case "ListPathSubSection":
			RowStatus = ids.Scan(&path, &app)
			DebugOutput("API-Methods:IDS:-> ", RowStatus)
			sqldata = map[string]interface{}{
				"Sections": path,
				"APP":      app,
			}
		case "PathMATCH", "ListFullSearch", "ListInstantSearch":
			RowStatus = ids.Scan(&path, &timecreate, &timelastedit, &app, &title1, &title2, &text1, &text2, &tags1, &num1, &num2, &num3, &NoSqlData, &public, &score)
			DebugOutput("API-Methods:IDS:-> ", RowStatus, path)
			sqldata = map[string]interface{}{
				"Path":         path,
				"Timecreate":   timecreate,
				"Timelastedit": timecreate,
				"APP":          app,
				"Title1":       title1,
				"Title2":       title2,
				"Text1":        text1,
				"Text2":        text2,
				"Tags1":        tags1,
				"Num1":         num1,
				"Num2":         num2,
				"Num3":         num3,
				"NoSqlData":    NoSqlData,
				"Public":       public,
				"Score":        score,
				"RowStatus":    RowStatus,
			}

		default:
			RowStatus = ids.Scan(&path, &timecreate, &timelastedit, &app, &title1, &title2, &text1, &text2, &tags1, &num1, &num2, &num3, &NoSqlData, &public)
			DebugOutput("API-Methods:IDS:-> ", RowStatus, path)
			sqldata = map[string]interface{}{
				"Path":         path,
				"Timecreate":   timecreate,
				"Timelastedit": timecreate,
				"APP":          app,
				"Title1":       title1,
				"Title2":       title2,
				"Text1":        text1,
				"Text2":        text2,
				"Tags1":        tags1,
				"Num1":         num1,
				"Num2":         num2,
				"Num3":         num3,
				"NoSqlData":    NoSqlData,
				"Public":       public,
				"RowStatus":    RowStatus,
			}

		}

		data = append(data, sqldata)

	}

	jsonres.DATA = map[string]interface{}{
		"List": data,
	}

	return
}
