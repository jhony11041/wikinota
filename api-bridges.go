package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type MultiReqStruct struct {
	MultiAPI []ReqStruct
}

//MultiHttpBridge is a Bridge for multiple API Requests
var MultiHttpBridge = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	startAPI := time.Now() //Take timestamp fo time-masurement

	var MultiReqStructdata MultiReqStruct
	errMultiReqStructdata := json.NewDecoder(r.Body).Decode(&MultiReqStructdata) //decode bpdy into json struct ApiReqStruct

	if errMultiReqStructdata != nil { //chek if json was valid
		errstring := `{"Status":"ERROR - NO VALID JSON -'` + errMultiReqStructdata.Error() + `' "}`
		fmt.Fprintf(w, errstring)
		fmt.Println(errMultiReqStructdata)
		checkErr(errMultiReqStructdata)
		return
	}

	var APIAnswerTMP []ResStruct

	for i, v := range MultiReqStructdata.MultiAPI {
		APIAnswerTMP = append(APIAnswerTMP, Handler(v)) //processing the request
		APIAnswerTMP[i].ElapsedLogicTime = time.Since(startAPI)
	}

	json.NewEncoder(w).Encode(APIAnswerTMP) //encode to json

	fmt.Println("MultiApiHandler:", time.Since(startAPI)) //output time Measurementm
	return
})

var HttpBridge = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var err error
	startAPI := time.Now() //Take timestamp fo time-masurement

	var jsondata ReqStruct //JSON need to fit in the ApiReqStruct structure

	errJson := json.NewDecoder(r.Body).Decode(&jsondata) //decode bpdy into json struct ApiReqStruct

	if errJson != nil { //chek if json was valid
		fmt.Fprintf(w, `{"Status":"ERROR - NO VALID JSON"}`)
		fmt.Println(errJson)
		checkErr(err)
		return
	}

	APIAnswerTMP := Handler(jsondata) //processing the request
	APIAnswerTMP.ElapsedLogicTime = time.Since(startAPI)

	json.NewEncoder(w).Encode(APIAnswerTMP) //encode to json

	fmt.Println("ApiHandler:", time.Since(startAPI)) //output time Measurementm
})
